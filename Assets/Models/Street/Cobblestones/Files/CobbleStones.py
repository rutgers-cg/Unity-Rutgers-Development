cameras = {}
scene = bpy.context.scene
frame = scene.frame_current - 1

data = bpy.data.cameras.new('Camera')
data.lens = 35.0
data.shift_x = 0.0
data.shift_y = 0.0
data.dof_distance = 0.0
data.clip_start = 0.10000000149011612
data.clip_end = 100.0
data.draw_size = 0.5
obj = bpy.data.objects.new('Camera', data)
obj.hide_render = False
scene.objects.link(obj)
cameras['Camera'] = obj

# new frame
scene.frame_set(1 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(2 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(3 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(4 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(5 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(6 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(7 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(8 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(9 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(10 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(11 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(12 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(13 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(14 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(15 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(16 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(17 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(18 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(19 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(20 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(21 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(22 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(23 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(24 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(25 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(26 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(27 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(28 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(29 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(30 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(31 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(32 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(33 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(34 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(35 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(36 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 35.0
data.keyframe_insert('lens')

# new frame
scene.frame_set(37 + frame)
obj = cameras['Camera']
obj.location = 0.156963512301445, -25.401063919067383, 8.177978515625
obj.scale = 1.0, 1.0, 0.9999998807907104
obj.rotation_euler = 1.1974806785583496, -6.208254035300342e-06, 0.027744945138692856
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
