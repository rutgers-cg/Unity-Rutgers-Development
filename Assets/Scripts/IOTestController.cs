﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NPC;
using TreeSharpPlus;

public class IOTestController : MonoBehaviour {

    public NPCController agentA;
    public NPCController agentB, agentC, agentD;
    public Transform[] WanderPoints;
    private NPCBehavior behaviorAgentA;
    private NPCBehavior behaviorAgentB;
    public NPCObject Object;
    public NPCObject Sit;

    private Node Behavior_GoTo(Vector3 point) {
        return behaviorAgentA.NPCBehavior_GoTo(point);
    }

    public void Awake() {
        Application.targetFrameRate = 24;
    }

    public void Start() {
        behaviorAgentA = agentA.GetComponent<NPCBehavior>();
        NPCBehavior behaviorAgentB = agentB.GetComponent<NPCBehavior>();
        NPCBehavior behaviorAgentC = agentC.GetComponent<NPCBehavior>();
        NPCBehavior behaviorAgentD = agentD.GetComponent<NPCBehavior>();
        
        behaviorAgentB.StartEvent(behaviorAgentB.NPCBehavior_CasualConversation(behaviorAgentB, behaviorAgentC), false,
            new IHasBehaviorObject[] { behaviorAgentB, behaviorAgentC });
        if (Sit != null)
            behaviorAgentD.StartEvent(behaviorAgentD.NPCBehavior_TakeSit(Sit));
    }

}
