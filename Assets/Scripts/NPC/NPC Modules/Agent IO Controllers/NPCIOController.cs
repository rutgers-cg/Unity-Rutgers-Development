﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using NPC;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(NPCController), typeof(NPCBehavior))]
[ExecuteInEditMode]
public class NPCIOController : MonoBehaviour, INPCModule {
    
    #region Enums

    public enum IO_TYPE {
        THIRD_PERSON
    }

    public enum KEYBOARD_INPUT {
        TOGGLE_NAVIGATION = KeyCode.F1,
        FORWARD = KeyCode.W,
        BACKWARDS = KeyCode.S,
        CAM_ROT_LEFT = KeyCode.Q,
        CAM_ROT_RIGHT = KeyCode.E,
        TURN_LEFT = KeyCode.A,
        TURN_RIGHT = KeyCode.D,
        RUN = KeyCode.LeftShift,
        JUMP = KeyCode.Space,
        CROUCH = KeyCode.LeftControl
    }

    public enum MOUSE_INPUT {
        INTERACT = KeyCode.Mouse0,
        FLOAT_CAMERA = KeyCode.Mouse3,
        WALK_TO = KeyCode.Mouse1

    }

    #endregion

    #region Members

    [SerializeField]
    public bool Enabled = true;

    [SerializeField]
    public IO_TYPE IOMode;

    [SerializeField]
    public bool EnableAgentCamera = true;

    [SerializeField]
    public bool EnableMouseCameraRotation = true;

    [SerializeField]
    public bool CaptureCurrentCameraOffset = false;

    [SerializeField]
    public float UpdateSmoothing = 2f;

    [SerializeField]
    public float CameraDistanceTolerance = 0.05f;

    [SerializeField]
    public Vector3 CameraOffset;
    
    [SerializeField]
    public float CameraRotationThreshold = 1f;

    [SerializeField]
    public float CameraRotationSpeed;

    [SerializeField]
    public float CameraTargetHeight = 1.2f;

    [SerializeField]
    public InteractionMapping[] InteractionsMap;

    [SerializeField]
    private NPCController g_NPCController;

    [SerializeField]
    public GameObject InteractionsPanel;


    private Dictionary<GameObject, InteractionMapping> g_InteractionsMap;
    private bool g_Initialized = false;
    private Vector3 g_MouseLastPosition;
    private Transform g_Camera;
    private NPCObject g_FocusedObject;
    private bool g_ObjectFocused = false;
    private NPCBehavior g_BehaviorAgent;

    private const string Interaction_Grab = "Interaction_Grab";
    private const string Interaction_Sit = "Interaction_Sit";
    private const string Interaction_Operate = "Interaction_Operate";
    private const string Interaction_Consume = "Interaction_Consume";

    #endregion

    #region Unity_Methods

    // Update is called once per frame
    void Update() {
        if (Application.isPlaying) {
            if (Enabled && g_Initialized) {
                UpdateCamera();
                UpdateInput();
            }
        } else {
            if(CaptureCurrentCameraOffset) {

                switch (IOMode) {
                    case IO_TYPE.THIRD_PERSON:
                        CameraOffset = Camera.main.transform.position - g_NPCController.GetTransform().position;
                        break;
                }

                CaptureCurrentCameraOffset = false;
            }
        }
    }

    public void Reset() {
        g_NPCController = GetComponent<NPCController>();
    }

    #endregion

    #region INPCModule

    public void CleanupModule() {
        StopCoroutine(MenuListener());
    }

    public void InitializeModule() {
        g_BehaviorAgent = GetComponent<NPCBehavior>();
        g_Camera = Camera.main.transform;
        g_MouseLastPosition = Input.mousePosition;
        g_Camera.forward = Vector3.Normalize(
            g_NPCController.GetTransform().position - g_Camera.position + new Vector3(0, CameraTargetHeight, 0));
        CameraOffset = Camera.main.transform.position - g_NPCController.GetTransform().position;
        StartCoroutine(MenuListener());
        if (InteractionsPanel != null) {
            foreach (Image go in InteractionsPanel.GetComponents<Image>()) {
                Color c = go.color;
                c.a -= 0.05f;
                go.color = c;
            }
            InteractionsPanel.SetActive(false);
            g_InteractionsMap = new Dictionary<GameObject, InteractionMapping>();
            foreach (InteractionMapping im in InteractionsMap) {
                g_InteractionsMap.Add(im.InteractionIcon,im);
            }
        }
        g_Initialized = true;
    }

    public bool IsEnabled() {
        return Enabled;
    }

    public bool IsUpdateable() {
        return false;
    }

    public string NPCModuleName() {
        return "NPC IO Controller";
    }

    public NPC_MODULE_TARGET NPCModuleTarget() {
        return NPC_MODULE_TARGET.CONTROLS;
    }

    public NPC_MODULE_TYPE NPCModuleType() {
        return NPC_MODULE_TYPE.IO_CONTROL;
    }

    public void RemoveNPCModule() {
        CleanupModule();
    }

    public void SetEnable(bool e) {
        Enabled = e;
    }

    public void TickModule() { }

    #endregion

    #region Private_Function

    protected IEnumerator MenuListener() {
        while(true) {
            if(g_ObjectFocused) {
                if(!Input.GetKey((KeyCode) MOUSE_INPUT.INTERACT)) {
                    StartCoroutine(FadeContextMenu(false));
                    g_ObjectFocused = false;
                    g_NPCController.Body.StopLookAt();
                }
            }
            yield return null;
        }
    }

    private void UpdateCamera() {
        if (EnableAgentCamera) {
            Vector3 agentPosition = g_NPCController.GetTransform().position;
            float distance = Vector3.Distance(agentPosition, Camera.main.transform.position);
            if (distance > CameraDistanceTolerance) {
                g_Camera.position = g_NPCController.GetTransform().position + CameraOffset;
            }
            g_Camera.forward = (g_NPCController.GetPosition() + new Vector3(0, CameraTargetHeight, 0) + (g_NPCController.GetTransform().forward * 0.5f)) - g_Camera.position;
        }
    }

    private void UpdateInput() {
        foreach (MOUSE_INPUT kc in Enum.GetValues(typeof(MOUSE_INPUT))) {

            if(Input.GetKeyUp((KeyCode) kc)) {
                switch(kc) {
                    case MOUSE_INPUT.INTERACT:
                        RaycastHit2D hit = Physics2D.Raycast(Input.mousePosition, Vector3.zero);
                        if (hit.collider != null) {
                            if(g_InteractionsMap.ContainsKey(hit.collider.gameObject)) {
                                string tag = g_InteractionsMap[hit.collider.gameObject].Tag;
                                switch (tag) {
                                    case Interaction_Grab:
                                        g_BehaviorAgent.StartEvent(g_BehaviorAgent.NPCBehavior_Grab(g_FocusedObject), true);
                                        break;
                                    case Interaction_Consume:
                                        break;
                                    case Interaction_Sit:
                                        g_BehaviorAgent.StartEvent(g_BehaviorAgent.NPCBehavior_TakeSit(g_FocusedObject), true);
                                        break;
                                    default:
                                        Debug.Log("No Interaction Tag: " + hit.transform.gameObject + " for object: " + g_FocusedObject);
                                        break;
                                }
                            }
                            
                        }
                        break;
                }
            }
            else if(Input.GetKey((KeyCode) kc)) {
                switch(kc) {
                    case MOUSE_INPUT.WALK_TO:
                        RaycastHit locationHit;
                        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out locationHit, 100.0f)) {
                            g_BehaviorAgent.StartEvent(g_BehaviorAgent.NPCBehavior_GoTo(locationHit.point), true);
                        }
                        break;
                    case MOUSE_INPUT.FLOAT_CAMERA:
                        if (EnableMouseCameraRotation) {
                            float horChange = Input.mousePosition.x - g_MouseLastPosition.x;
                            float verChange = Input.mousePosition.y - g_MouseLastPosition.y;
                            bool correct = false;
                            if (Mathf.Abs(horChange) > CameraRotationThreshold) {
                                int mod = horChange > 0 ? 1 : -1;
                                g_Camera.RotateAround(g_NPCController.GetPosition(), mod * Vector3.up, CameraRotationSpeed * Time.deltaTime);
                                correct = true;
                            }
                            if (Mathf.Abs(verChange) > CameraRotationThreshold) {
                                int mod = verChange > 0 ? -1 : 1;
                                Vector3 rotationAxis =
                                    Vector3.Cross(
                                        g_Camera.forward,
                                        Vector3.up);
                                g_Camera.RotateAround(g_NPCController.GetPosition(), rotationAxis, mod * CameraRotationSpeed * Time.deltaTime);
                                correct = true;
                            }
                            if (correct) {
                                CameraOffset = Camera.main.transform.position - g_NPCController.GetTransform().position;
                            }
                        }
                        break;
                    case MOUSE_INPUT.INTERACT:
                        if (!g_ObjectFocused) {
                            RaycastHit hit;
                            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                            if (Physics.Raycast(ray, out hit, 100.0f)) {
                                NPCObject npcObject = hit.transform.GetComponent<NPCObject>();
                                if (npcObject != null) {
                                    g_FocusedObject = npcObject;
                                    g_ObjectFocused = true;
                                    g_NPCController.Body.StartLookAt(npcObject.GetTransform());
                                    StartCoroutine(FadeContextMenu(true, g_FocusedObject));
                                }
                            }
                        }
                        break;
                }
                g_MouseLastPosition = Input.mousePosition;
            }
        }

        foreach (KEYBOARD_INPUT key in Enum.GetValues(typeof(KEYBOARD_INPUT))) {
            if (Input.GetKey((KeyCode) key)) {
                switch (key) {
                    case KEYBOARD_INPUT.FORWARD:
                        g_NPCController.Body.Move(LOCO_STATE.FORWARD);
                        break;
                    case KEYBOARD_INPUT.BACKWARDS:
                        g_NPCController.Body.Move(LOCO_STATE.BACKWARDS);
                        break;
                    case KEYBOARD_INPUT.CAM_ROT_LEFT:
                        g_Camera.RotateAround(g_NPCController.GetPosition(), Vector3.up, CameraRotationSpeed * Time.deltaTime);
                        CameraOffset = Camera.main.transform.position - g_NPCController.GetTransform().position;
                        break;
                    case KEYBOARD_INPUT.CAM_ROT_RIGHT:
                        g_Camera.RotateAround(g_NPCController.GetPosition(), -1 * Vector3.up, CameraRotationSpeed * Time.deltaTime);
                        CameraOffset = Camera.main.transform.position - g_NPCController.GetTransform().position;
                        break;
                    case KEYBOARD_INPUT.TURN_LEFT:
                        g_NPCController.Body.Move(LOCO_STATE.LEFT);
                        break;
                    case KEYBOARD_INPUT.TURN_RIGHT:
                        g_NPCController.Body.Move(LOCO_STATE.RIGHT);
                        break;
                    case KEYBOARD_INPUT.RUN:
                        g_NPCController.Body.Move(LOCO_STATE.RUN);
                        break;
                }
            }
        }
    }

    private IEnumerator FadeContextMenu(bool fadeIn, NPCObject obj = null) {
        InteractionsPanel.SetActive(fadeIn);
        float alpha = fadeIn ? 0f : 1f;
        if(fadeIn) {
            SetContextMenuEnabled(true);
            while(alpha < 0.9f) {
                InteractionsPanel.transform.position = Camera.main.WorldToScreenPoint(g_FocusedObject.GetPosition());
                foreach (Image go in InteractionsPanel.GetComponents<Image>()) {
                    Color c = go.color;
                    c.a += 0.05f;
                    alpha = c.a;
                    go.color = c;
                }
                yield return null;
            }
            
        } else {
            while (alpha > 0f) {
                foreach (Image go in InteractionsPanel.GetComponents<Image>()) {
                    Color c = go.color;
                    c.a -= 0.05f;
                    alpha = c.a;
                    go.color = c;
                }
                yield return null;
            }
            SetContextMenuEnabled(false);
        }
    }

    private void SetContextMenuEnabled(bool b) {
        foreach (Image go in InteractionsPanel.GetComponents<Image>()) {
            go.gameObject.SetActive(b);
        }
    }

    public void OnMouseUp() {
        Debug.Log("Mouse up");
    }

    [Serializable]
    public class InteractionMapping {
        public GameObject InteractionIcon;
        public string Tag;
        public INTERACTION_TYPE Interaction;
    }

    #endregion

}
