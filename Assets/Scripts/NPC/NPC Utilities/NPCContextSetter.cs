﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NPC {

    public class NPCContextSetter : MonoBehaviour {

        public void OnTriggerStay(Collider collider) {
            INPCPerceivable c = collider.gameObject.GetComponent<INPCPerceivable>();
            if (c != null)
                c.SetCurrentContext(gameObject.name);
        }

        public void OnTriggerExit(Collider collider) {
            INPCPerceivable c = collider.gameObject.GetComponent<INPCPerceivable>();
            if (c != null)
                c.SetCurrentContext(null);
        }

    }

}
