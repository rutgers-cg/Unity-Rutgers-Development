﻿using UnityEngine;
using System.Collections;
using UnityEditor.Animations;
using System.Collections.Generic;
using System;
using System.Reflection;
using UnityEngine.AI;

namespace NPC {

    [System.Serializable]
    public class NPCBody : MonoBehaviour {

        #region Members

        [SerializeField]
        private NavMeshAgent gNavMeshAgent;

        [SerializeField]
        private Animator g_Animator;

        [SerializeField]
        private NPCIKController gIKController;

        private Rigidbody gRigidBody;
        private CapsuleCollider gCapsuleCollider;
        private bool g_LookingAround = false;
        private Vector3 g_TargetLocation;
        private Vector3 g_LastpdatedPosition;

        // Timed gestures / IK controller
        private NPCTimer g_GestureTimer;

        private static string g_AnimParamSpeed = "Speed";
        private static string g_AnimParamDirection = "Direction";
        private static string g_AnimParamJump = "Jump";
        private static string g_AnimParamColliderFactor = "Collider_Factor";

        private static int   SPEED_MOD = 2;
        private static float MAX_WALK_SPEED = 1.00f;
        private static float MAX_RUN_SPEED = 1.00f * SPEED_MOD;
        private static float MIN_WALK_SPEED = -1 * MAX_WALK_SPEED;
        private static float MIN_RUN_SPEED = -1 * MAX_WALK_SPEED;

        private static float MAX_TURNING_ANGLE = 180f;

        private static Dictionary<GESTURE_CODE, NPCAnimation> m_Gestures;
        private GESTURE_CODE LastGesture;
        private static Dictionary<NPCAffordance, string> m_Affordances;

        private LOCO_STATE g_CurrentStateFwd = LOCO_STATE.IDLE;
        private LOCO_STATE g_CurrentStateGnd = LOCO_STATE.GROUND;
        private LOCO_STATE g_CurrentStateDir = LOCO_STATE.FRONT;
        private LOCO_STATE g_CurrentStateMod = LOCO_STATE.WALK;

        // This correlate with the parameters from the Animator
        private bool g_RunForce = false;
        private bool g_Blocked = false;
        private float g_CurrentSpeed = 0.0f;
        private float g_CurrentVelocity = 0.05f;
        private float g_CurrentOrientation = 0.0f;
        private float g_TurningVelocity;
        private float g_OrientationAngle = 0f;
        private float g_TopFowardSpeed = 1f;

        // for feet correction
        private float g_ColliderHeight;
        private bool g_TargetLocationReached = true;
        private static int gHashJump = Animator.StringToHash("JumpLoco");
        private static int gHashIdle = Animator.StringToHash("Idle");
        private Vector3 g_TargetOrientation;                                // Wheres the NPC currently looking at
        
        // Animations
        private Dictionary<string,AnimationClip> g_AnimationStates;
        private Dictionary<string, NPCAnimatedAudio> g_AnimatedAudioClips;
        private float g_NextBlink;

        // Navigation
        private List<Vector3> g_NavQueue;
        private bool OverrideMaxSpeedValue = false;
        private NPCController g_NPCController;
        [System.ComponentModel.DefaultValue(1f)]
        private float MaxWalkSpeed { get; set; }
        [System.ComponentModel.DefaultValue(2f)]
        private float MaxRunSpeed { get; set; }
        private float g_DistanceToTarget;

        #endregion

        #region Properties

        public float TurningAngleThreshold = 5; // Orientation threshold

        public Animator Animator {
            get {
                return g_Animator;
            }
        }
        
        public List<Vector3> NavigationPath {
            get {
                return g_NavQueue;
            }
        }

        /* STEERING / WALKING PARAMS */

        public Vector3 TargetLocation {
            get {
                // return g_TargetLocation;
                return Navigating ?
                    g_NavQueue[g_NavQueue.Count - 1] : g_TargetLocation;
            }
            set {
                g_TargetLocation = value;
            }
        }

        public Vector3 TargetOrientation {
            get {
                return g_TargetOrientation;
            }
        }
        
        [SerializeField] public float LocomotionBlendSpeed = 30f;
        [SerializeField] public float TurningBlendSpeed = 4.5f;
        [SerializeField] public bool  ObstaclesDetectionEnabled = true;
        [SerializeField] public float StepHeight = 0.3f;
        [SerializeField] public float MaxStepHeight = 0.6f;
        [SerializeField] public float TurningAngle = 55f;
        [SerializeField] public float MinTurningAngle = 40f;
        [SerializeField] public float MaxTurningAngle = 90f;
        [SerializeField] public float StepColliderHeightCorrection = 0.1f;
        [SerializeField] public float ColliderRadiusCorrection = 0.1f;
        [SerializeField] public float ColliderTestDistance = 0.1f;
        [SerializeField] public float ColliderTestAngle = 0.1f;
        [SerializeField] public float HeightAdjustInterpolation = 5f;
        [SerializeField] public float ForwardHeightAdjustment = 0.2f;
        [SerializeField] public float MaxArmsReach = 0.7f;


        /* SOCIAL FORCES */
        [SerializeField] public bool EnableSocialForces;
        [SerializeField] public bool EnableVelocityModifier;
        [SerializeField] public float VelocityModifierScale = 1f;
        [SerializeField] public float AgentAttractionForce = 0f;
        [SerializeField] public float AgentRepulsionWeight = 0.3f;
        [SerializeField] public float DistanceTolerance = 0.8f;
        [SerializeField] public float ProximityScaleMultiplier = 3f;
        [SerializeField] public float FollowAgentsFlow = 0f;

        /* PHYSICS */

        /// <summary>
        /// Enabled when a single collider is used for the entire agents body.
        /// Otherwise, it is assumed a Ragdoll or other colliders, manually controlled are
        /// used instead.
        /// </summary>
        [SerializeField]
        public bool UseSingleCapsuleCollider = false;

        public float Mass {
            get {
                return gRigidBody.mass;
            }
        }

        public float AgentRadius {
            get {
                return gCapsuleCollider.radius;
            }
        }

        public Vector3 Velocity {
            get {
                return (transform.position - g_LastpdatedPosition) * Time.deltaTime;
            }
        }

        public float Speed {
            get {
                return g_CurrentSpeed;
            }
        }

        public float Orientation {
            get {
                return g_CurrentOrientation;
            }
        }

        public bool Blocked {
            get {
                return g_Blocked;
            }
        }

        public Vector3 TargetDirection {
            get {
                return g_TargetOrientation;
            }
        }

        public bool Navigating = false;

        public bool Oriented = false;
        
        [SerializeField]
        public NAV_STATE Navigation;

        [SerializeField]
        public bool UseCurves;

        [SerializeField]
        public bool EnableBlinking;

        [SerializeField]
        public float BlinkingSpeed = 2f;

        /* IK */
        [SerializeField] public bool IKEnabled;
        [SerializeField] public bool IK_FEET_Enabled;
        [SerializeField] public float IK_FEET_HEIGHT_CORRECTION;
        [SerializeField] public float IK_FEET_FORWARD_CORRECTION;
        [SerializeField] public float IK_FEET_HEIGHT_EFFECTOR_CORRECTOR;
        [SerializeField] public float IK_FEET_STAIRS_INTERPOLATION;
        [SerializeField] public bool IK_USE_HINTS = true;
        [SerializeField] public float IK_START_LOOK_AT_ADJUST = 1f;
        [SerializeField] public float IK_LOOK_AT_SMOOTH;
        [SerializeField] public float IK_RIGHT_HAND_SMOOTH;
        [SerializeField] public float IK_LEFT_HAND_SMOOTH;
        [SerializeField] public bool UseAnimatorController;
        [SerializeField] public float NavDistanceThreshold = 0.3f;

        public bool LookingAround {
            get {
                return g_LookingAround;
            }
        }

        public Transform TargetObject {
            get {
                return gIKController.LOOK_AT;
            }
        }

        public Transform Head {
            get {
                return gIKController.Head;
            }
        }

        public bool IsTimedGesturePlaying(GESTURE_CODE gest) {
            return !g_GestureTimer.Finished;
        }

        public bool IsAtTargetLocation(Vector3 targetLoc) {
            return g_TargetLocationReached &&
                Vector3.Distance(targetLoc, transform.position) <= NavDistanceThreshold
                * (g_NPCController.PerceivedAgents.Count == 0 ? 1 : g_NPCController.PerceivedAgents.Count);
        }

        public bool IsIdle {
            get {
                return
                    // TODO - this should test for navigation and state, not animation state
                    // We always need to test for a state and a possible active transition
                    g_Animator.GetCurrentAnimatorStateInfo(0).shortNameHash == gHashIdle
                    && g_Animator.GetAnimatorTransitionInfo(0).fullPathHash == 0;
            }
        }
        #endregion

        #region Unity_Methods

        void Reset() {
            g_NPCController = GetComponent<NPCController>();
            g_NPCController.Debug("Initializing NPCBody ... ");
            gNavMeshAgent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
            gRigidBody = gameObject.GetComponent<Rigidbody>();
            g_Animator = gameObject.GetComponent<Animator>();
            gIKController = gameObject.GetComponent<NPCIKController>();
            if (gIKController == null) gIKController = gameObject.AddComponent<NPCIKController>();
            gIKController.hideFlags = HideFlags.HideInInspector;
            if (gNavMeshAgent == null) {
                gNavMeshAgent = gameObject.AddComponent<UnityEngine.AI.NavMeshAgent>();
                gNavMeshAgent.autoBraking = true;
                gNavMeshAgent.enabled = false;
                g_NPCController.Debug("NPCBody requires a NavMeshAgent if navigation is on, adding a default one.");
            }
            if (g_Animator == null || g_Animator.runtimeAnimatorController == null) {
                g_NPCController.Debug("NPCBody --> Agent requires an Animator Controller!!! - consider adding the NPCDefaultAnimatorController");
            } else UseAnimatorController = true;
            if (gRigidBody == null) {
                gRigidBody = gameObject.AddComponent<Rigidbody>();
                gRigidBody.useGravity = true;
                gRigidBody.mass = 3;
                gRigidBody.constraints = RigidbodyConstraints.FreezeRotation;
            }
            
            // Initialize main collider if none exists
            gCapsuleCollider = gameObject.GetComponentInChildren<CapsuleCollider>();
            if (gCapsuleCollider == null) {
                UseSingleCapsuleCollider = true;
                gCapsuleCollider = gameObject.AddComponent<CapsuleCollider>();
                gCapsuleCollider.radius = 0.2f;
                gCapsuleCollider.height = 1.8f;
                gCapsuleCollider.center = new Vector3(0.0f, gCapsuleCollider.height / 2f, 0.0f);
            }

            if (gIKController == null) {
                gIKController = gameObject.AddComponent<NPCIKController>();
            }

            g_NPCController.EntityType = PERCEIVEABLE_TYPE.NPC;
        }

        #endregion

        #region Public_Funtions

        public void InitializeBody() {

            g_NPCController = GetComponent<NPCController>();

            // For handling height shifts - i.e. stairs
            if (UseSingleCapsuleCollider) {
                if (GetComponentInChildren<CharacterJoint>() != null) {
                    UseSingleCapsuleCollider = false;
                    g_NPCController.Debug("Ragdoll joints detected, disabling Use Single Capsule Collider");
                } else {
                    gCapsuleCollider = gameObject.GetComponent<CapsuleCollider>();
                    g_ColliderHeight = GetComponent<CapsuleCollider>().height;
                }
            }

            // Initialize static members for all NPC - this assumes all Agents use the same animator for its meta data
            if (NPCBody.m_Gestures == null) {
                InitializeGestures();
            }
            if (NPCBody.m_Affordances == null) {
                InitializeAffordances();
            }

            g_Animator = gameObject.GetComponent<Animator>();
            gIKController = gameObject.GetComponent<NPCIKController>();
            gNavMeshAgent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
            gRigidBody = GetComponent<Rigidbody>();
            if (g_Animator == null || gNavMeshAgent == null) UseAnimatorController = false;
            if (gIKController == null) IKEnabled = false;
            g_NavQueue = new List<Vector3>();
            if (g_NPCController.TestTargetLocation != null) {
                GoTo(new List<Vector3>() { g_NPCController.TestTargetLocation.position });
            }
            g_TargetLocation = transform.position;
            g_TargetOrientation = transform.forward;
            g_GestureTimer = new NPCTimer();

            if (NPCBody.m_Gestures == null) {
                InitializeGestures();
            }

            // Index Animated Audio Clips
            g_AnimatedAudioClips = new Dictionary<string, NPCAnimatedAudio>();
            foreach (NPCAnimatedAudio a in g_NPCController.AnimatedAudioClips) {
                if (string.IsNullOrEmpty(a.Name)) {
                    g_NPCController.Debug("An animated clip hasn't had a name assigned to it.");
                } else {
                    a.BakeAnimatedAudioClip(m_Gestures);
                    g_AnimatedAudioClips.Add(a.Name, a);
                }
            }

            if (EnableBlinking) {
                g_NextBlink = Time.time + (UnityEngine.Random.value * BlinkingSpeed);
            }

            g_TurningVelocity = TurningAngleThreshold / MAX_TURNING_ANGLE;
        }

        public void OverrideMaxSpeedValues(bool activate, float walk, float run) {
            OverrideMaxSpeedValue = activate;
            MaxRunSpeed = OverrideMaxSpeedValue  ? run  : MAX_RUN_SPEED;
            MaxWalkSpeed = OverrideMaxSpeedValue ? walk : MAX_WALK_SPEED;
        }

        public NPCAnimation Animation(GESTURE_CODE g) {
            return m_Gestures[g];
        }

        public void UpdateBody() {

            if (IKEnabled) {
                gIKController.UpdateIK();
            }

            if(EnableBlinking) {
                if(Time.time > g_NextBlink) {
                    DoGesture(GESTURE_CODE.BLINK);
                    g_NextBlink = Time.time + (BlinkingSpeed * UnityEngine.Random.value);
                }
            }

            /*
             *  Every agent, main agent (main character) or not will constantly update their
             *  steering and orientation if needed. This is necessary due to the fact that
             *  we need to allow controllers to interface via the NPCController body accessors
             *  in different ways.
             */

            UpdateNavigation();
            UpdateOrientation();
            if(ObstaclesDetectionEnabled)
                UpdateObstaclesDetection();

            g_LastpdatedPosition = transform.position;

            if (UseAnimatorController) {

                // Update gestures timer
                if (!g_GestureTimer.Finished)
                    g_GestureTimer.UpdateTimer();

                // If accidentally checked on set up
                if (g_Animator == null) {
                    g_NPCController.Debug("NPCBody --> No Animator in agent, disabling UseAnimatorController");
                    UseAnimatorController = false;
                    return;
                }

                // Is the agent running while navigating
                if (g_RunForce) Move(LOCO_STATE.RUN);

                // handle mod
                float forth = g_CurrentStateFwd == LOCO_STATE.FORWARD ? 1.0f : -1.0f;
                float orient = g_CurrentStateDir == LOCO_STATE.RIGHT ? 1.0f : -1.0f;
                bool duck = (g_CurrentStateMod == LOCO_STATE.DUCK);

                if (OverrideMaxSpeedValue) {
                    g_TopFowardSpeed = g_CurrentStateMod == LOCO_STATE.RUN
                        ? MaxRunSpeed : MaxWalkSpeed;
                } else {
                    g_TopFowardSpeed = g_CurrentStateMod == LOCO_STATE.RUN
                        ? MAX_RUN_SPEED : MAX_WALK_SPEED;
                }

                // update forward
                float deltaVal = g_CurrentSpeed;
                if(g_CurrentStateFwd != LOCO_STATE.IDLE) {
                    g_CurrentSpeed =
                        Mathf.Lerp(g_CurrentSpeed, forth * g_TopFowardSpeed, Time.deltaTime * LocomotionBlendSpeed);
                } else {
                    g_CurrentSpeed =
                        Mathf.Lerp(g_CurrentSpeed, 0f, Time.deltaTime * LocomotionBlendSpeed);
                }
                
                // update direction
                if (g_CurrentStateDir != LOCO_STATE.FRONT) {
                    g_CurrentOrientation = Mathf.Lerp(g_CurrentOrientation,
                        orient * 1f, Time.deltaTime * TurningBlendSpeed);
                } else {
                    g_CurrentOrientation = Mathf.Lerp(g_CurrentOrientation, 0f, Time.deltaTime * TurningBlendSpeed * 2f);
                }

                // update ground
                if (g_CurrentStateGnd == LOCO_STATE.JUMP) {
                    g_Animator.SetTrigger(g_AnimParamJump);
                    g_CurrentStateGnd = LOCO_STATE.FALL;
                } else if (g_Animator.GetAnimatorTransitionInfo(0).fullPathHash == 0) {
                    // this is as long as we are not in jump state
                    g_CurrentStateGnd = LOCO_STATE.GROUND;
                }

                // apply curves if needed
                if (UseCurves) {
                    if(UseSingleCapsuleCollider)
                        GetComponent<CapsuleCollider>().height = g_ColliderHeight * g_Animator.GetFloat(g_AnimParamColliderFactor);
                }

                // set animator

                g_Animator.SetFloat(g_AnimParamSpeed, g_CurrentSpeed);
                g_Animator.SetFloat(g_AnimParamDirection, g_CurrentOrientation);

                // reset all states until updated again
                SetIdle();
            }
        }

        public void Move(LOCO_STATE s) {
            switch (s) {
                case LOCO_STATE.RUN:
                case LOCO_STATE.DUCK:
                case LOCO_STATE.WALK:
                    g_CurrentStateMod = s;
                    break;
                case LOCO_STATE.FORWARD:
                case LOCO_STATE.BACKWARDS:
                case LOCO_STATE.IDLE:
                    g_CurrentStateFwd = s;
                    break;
                case LOCO_STATE.RIGHT:
                case LOCO_STATE.LEFT:
                case LOCO_STATE.FRONT:
                    g_CurrentStateDir = s;
                    break;
                case LOCO_STATE.JUMP:
                    g_CurrentStateGnd = s;
                    break;
                default:
                    g_NPCController.Debug("NPCBody --> Invalid direction especified for ModifyMotion");
                    break;
            }
        }

        #region Affordances

        /// <summary>
        /// No path finding involved.
        /// </summary>
        /// <param name="location"></param>
        [NPCAffordance("WalkTowards")]
        public void WalkTowards(Vector3 location) {
            g_NavQueue.Clear();
            g_NavQueue.Add(location);
        }

        [NPCAffordance("RunTo")]
        public void RunTo(Vector3 t) {
            if (Vector3.Distance(t, TargetLocation)
                >= NavDistanceThreshold * 2f) {
                List<Vector3> path = g_NPCController.AI.FindPath(t);
                if (path.Count < 1) {
                    g_NPCController.Debug("NPCController --> No path found to target location");
                } else {
                    RunTo(path);
                }
            }
        }

        /// <summary>
        /// The queue will we checked and followed every UpdateNavigation call
        /// </summary>
        /// <param name="List of locations to follow"></param>
        [NPCAffordance("GoTo")]
        public void GoTo(Vector3 t) {
            float distance = Vector3.Distance(t, TargetLocation);
            if (distance
                >= NavDistanceThreshold) {
                List<Vector3> path = g_NPCController.AI.FindPath(t);
                if (path.Count < 1) {
                    g_NPCController.Debug("NPCController --> No path found to target location");
                } else {
                    GoTo(path);
                }
            }
        }

        [NPCAffordance("StopFollow")]
        public void StopFollow() {
            g_NPCController.FollowTarget = null;
        }

        [NPCAffordance("Follow")]
        public void Follow(Transform target, bool run) {
            g_NPCController.FollowTarget = target;
            List<Vector3> path = g_NPCController.AI.FindPath(target.position);
            if(run) {
                RunTo(path);
            } else {
                GoTo(path);
            }
        }

        /// <summary>
        /// Requires an orientation vector from the agent's position
        /// target - current position
        /// </summary>
        /// <param name="target"></param>
        [NPCAffordance("OrientTowards")]
        public void OrientTowards(Vector3 target) {
            g_TargetOrientation = target;
        }

        [NPCAffordance("StartLookAt")]
        public void StartLookAt(Transform t) {
            INPCPerceivable p = t.GetComponent<INPCPerceivable>();
            if (p != null) {
                gIKController.LOOK_AT = p.GetMainLookAtPoint();
            } else
                gIKController.LOOK_AT = t;
        }

        [NPCAffordance("StopLookAt")]
        public void StopLookAt() {
            gIKController.LOOK_AT = null;
        }

        /// <summary>
        /// The caller might specify an optional parameter depeding on the type of animation.
        /// The optional parameter could be a float or a boolean. Triggers do not require
        /// parameters.
        /// </summary>
        /// <param name="gesture"></param>
        /// <param name="o"></param>
        [NPCAffordance("DoGesture")]
        public void DoGesture(GESTURE_CODE gesture, System.Object o = null, bool timed = false) {
            NPCAnimation anim = m_Gestures[gesture];
            switch (anim.ParamType) {
                case ANIMATION_PARAM_TYPE.TRIGGER:
                    g_Animator.SetTrigger(anim.Name);
                    break;
                case ANIMATION_PARAM_TYPE.BOOLEAN:
                    bool b = (o == null ? !g_Animator.GetBool(anim.Name) : (bool) o);
                    g_Animator.SetBool(anim.Name, b);
                    break;
                case ANIMATION_PARAM_TYPE.FLOAT:
                    float f = (float)o;
                    g_Animator.SetFloat(anim.Name, f);
                    break;
            }
            LastGesture = gesture;
            if (timed)
                g_GestureTimer.StartTimer(m_Gestures[gesture].Duration);
        }

        /// <summary>
        /// Starts a coroutine which hanles synchronized anims and audio
        /// </summary>
        /// <param name="animAudioName"></param>
        [NPCAffordance("DoAudioAnimGesture")]
        public void DoAudioAnimGesture(string animAudioName) {
            if (g_AnimatedAudioClips.ContainsKey(animAudioName)) {
                StartCoroutine(PlayAnimatedAudioClip(g_AnimatedAudioClips[animAudioName]));
            } else {
                g_NPCController.Debug("Animated Audio Clip is not mapped for key: " + animAudioName);
            }
        }

        [NPCAffordance("StopNavigation")]
        public void StopNavigation() {
            g_NPCController.FollowTarget = null;
            g_RunForce = false;
            SetIdle();
            g_TargetOrientation = transform.forward;
            g_NavQueue.Clear();
            Navigating = false;
            g_TargetLocation = transform.position;
            g_TargetLocationReached = true;
        }

        [NPCAffordance("StopAllMotion")]
        public void StopAllMotion() {
            StopNavigation();
            g_GestureTimer.StopTimer();
        }

        [NPCAffordance("GrabRightHand")]
        public void GrabRightHand(NPCObject t, bool grab = true, GESTURE_CODE grabGest = GESTURE_CODE.GRAB_FRONT) {
            float dist = Vector3.Distance(t.GetPosition(), transform.position);
            if (grab && dist <= MaxArmsReach) {
                OrientTowards(t.GetPosition() - transform.position);
                gIKController.GRAB_RIGHT = t;
                if(dist < MaxArmsReach * 0.5f)
                    DoGesture(GESTURE_CODE.PICK_UP, true, true);
                else
                    DoGesture(grabGest, true, true);
            } else {
                gIKController.GRAB_RIGHT = null;
            }
            DoGesture(GESTURE_CODE.HAND_GRAB_RIGHT, grab);
        }

        /// <summary>
        /// Used to start and stop looking around
        /// </summary>
        /// <param name="startLooking"></param>
        public void LookAround(bool startLooking) {
            GameObject go;
            if (startLooking && !g_LookingAround) {
                go = new GameObject();
                go.name = "TmpLookAtTarget";
                Func<Vector3, Vector3> pos = np => (np + (1.50f * transform.forward));
                go.transform.position = pos(transform.position);
                go.transform.rotation = transform.rotation;
                go.transform.SetParent(transform);
                StartLookAt(go.transform);
                g_LookingAround = true;
            } else if (g_LookingAround) {
                go = gIKController.LOOK_AT.gameObject;
                StopLookAt();
                DestroyImmediate(go);
                g_LookingAround = false;
            }
        }
        
        #endregion

        #endregion

        #region Private_Functions

        private void RunTo(List<Vector3> location) {
            GoTo(location);
            g_RunForce = true;
        }

        private void GoTo(List<Vector3> location) {
            g_RunForce = false;
            Navigating = true;
            g_NavQueue.Clear();
            g_NavQueue = location;
            g_TargetLocationReached = false;
        }

        private void UpdateNavigation() {
            if (Navigation != NAV_STATE.DISABLED) {
                if (Navigation == NAV_STATE.STEERING_NAV) {
                    if (g_NavQueue.Count > 0 || g_NPCController.Following) {
                        // if following a target and no path is available, recalculate path
                        if (g_NPCController.AI.PathUpdateable) {
                            if (g_NPCController.Following) {
                                if (g_NavQueue.Count == 0)
                                    g_NavQueue = g_NPCController.AI.FindPath(g_NPCController.FollowTarget.position);
                                else if (Vector3.Distance(
                                    g_NPCController.FollowTarget.position, g_NavQueue[g_NavQueue.Count - 1]) > NavDistanceThreshold) {
                                    g_NavQueue = g_NPCController.AI.FindPath(g_NPCController.FollowTarget.position);
                                }
                            }
                        }
                        Navigating = g_NavQueue.Count > 0;
                        g_TargetLocationReached = false;
                        HandleSteering();
                    }
                } else {
                    g_TargetLocation = g_NavQueue[0];
                    g_NavQueue.Clear();
                    HandleNavAgent();
                }
            } else {
                Navigating = false;
            }
        }

        private void HandleNavAgent() {
            if (gNavMeshAgent != null) {
                if (!gNavMeshAgent.enabled)
                    gNavMeshAgent.enabled = true;
                if (g_NavQueue.Count > 0)
                    gNavMeshAgent.SetDestination(g_TargetLocation);
            }
        }

        private void HandleSteering() {
            if (!Navigating) return;
            g_TargetLocation = g_NavQueue[0];
            g_NPCController.DebugLine(transform.position, g_TargetLocation, Color.green);
            g_DistanceToTarget = Vector3.Distance(TargetLocation, transform.position);
            float distanceToNextPoint = Vector3.Distance(transform.position, g_TargetLocation);
            float threshold = NavDistanceThreshold;
            g_TargetOrientation = g_TargetLocation - transform.position;
            g_NPCController.DebugLine(Head.position, g_TargetLocation, Color.green);

            // TODO - there's gotta be a smarter way to control stopping, yet this one's working for now in volumes.
            int agentsCount = (g_NPCController.PerceivedAgents.Count == 0 ? 1 : g_NPCController.PerceivedAgents.Count);
            if ((g_DistanceToTarget <= threshold
                * agentsCount)
                && g_NavQueue.Count == 1) {
                RaycastHit h;
                if (Physics.SphereCast(Head.position, AgentRadius * agentsCount, g_TargetOrientation, out h,
                    threshold)) {
                    goto NextPoint;
                }
            }

            if (EnableSocialForces && g_NPCController.PerceivedAgents.Count > 0) {
                ComputeSocialForces(ref g_TargetOrientation);
                if (g_NPCController.DebugMode)
                    g_NPCController.DebugRay(transform.position + Vector3.up, g_TargetOrientation, Color.blue);
            }

            Move(LOCO_STATE.FORWARD);

            if (distanceToNextPoint >= threshold) {
                return;
            }

            NextPoint:
            if (Navigating)
                g_NavQueue.RemoveAt(0);
            Navigating = g_NavQueue.Count > 0;
            if(!Navigating) StopNavigation();
        }
        
        private void SetIdle() {
            g_CurrentStateFwd = LOCO_STATE.IDLE;
            g_CurrentStateGnd = LOCO_STATE.GROUND;
            g_CurrentStateDir = LOCO_STATE.FRONT;
            g_CurrentStateMod = LOCO_STATE.WALK;
        }

        private void ComputeSocialForces(ref Vector3 currentTarget) {

            currentTarget = Vector3.Normalize(currentTarget);
            float dist = Vector3.Distance(transform.position, TargetLocation);
            Vector3 followAgents = ComputeCohesionForce();
            Vector3 repulsionForce = ComputeAgentsRepulsionForce(currentTarget);
            Vector3 preferredForce = ((currentTarget - Velocity) * Speed) / dist;
            currentTarget = preferredForce + repulsionForce + followAgents;

        }

        private Vector3 ComputeCohesionForce() {
            Vector3 totalForces = Vector3.zero;
            float count = 0;
            foreach (INPCPerceivable p in g_NPCController.Perception.PerceivedAgents) {
                totalForces += p.GetPosition() * p.GetPerceptionWeight();
                count += p.GetPerceptionWeight();
            }
            if (count > 0) {
                totalForces /= count;
            }
            totalForces -= transform.position;
            return g_NPCController.PerceivedAgents.Count > 0 ?
                totalForces.normalized * Mathf.Min(totalForces.magnitude, 2) * AgentAttractionForce : Vector3.zero;
        }

        private Vector3 ComputeAgentsRepulsionForce(Vector3 currentTarget) {
            Vector3 totalForces = currentTarget,
                proximityForce = currentTarget,
                avgDirection = currentTarget,
                flowForce = currentTarget;
            int agents = g_NPCController.Perception.PerceivedAgents.Count;

            foreach (INPCPerceivable p in g_NPCController.Perception.PerceivedAgents) {

                Vector3 normal = (transform.position - p.GetPosition()).normalized;
                Vector3 direction = (Vector3.Dot(transform.right, -normal) < 0f ? transform.right : -transform.right);

                float radii = AgentRadius + p.GetAgentRadius();
                float distance = Vector3.Distance(transform.position, p.GetPosition());
                if (distance <= radii) {
                    totalForces += normal;
                    avgDirection += direction * AgentRepulsionWeight;
                }
                float faceAngle = Vector3.Dot(transform.forward, -normal);
                float targetOr = Vector3.Dot(transform.forward, currentTarget);
                if (faceAngle > 0.7f && targetOr > 0f) {
                    /* Compute Proximity */
                    float scale = 0f;

                    if (p.GetNPCEntityType() == PERCEIVEABLE_TYPE.NPC) {
                        scale = ProximityScaleMultiplier * Mathf.Exp(radii - (distance / DistanceTolerance));
                    } else {
                        // TODO: add the weights for objects - constant B
                        scale = ProximityScaleMultiplier * Mathf.Exp(radii - (distance / DistanceTolerance));
                    }

                    if (EnableVelocityModifier) {
                        g_CurrentSpeed = Mathf.Max(
                            g_CurrentSpeed - (scale * g_CurrentVelocity * VelocityModifierScale) * (1f + p.GetCurrentSpeed()),
                            0f);
                    }

                    /* Obtain avg direction */
                    avgDirection += (direction / distance);
                    proximityForce += (normal * scale);
                }
                /* Compute Agents Flow */
                if (p.GetCurrentVelocity() != Vector3.zero) {
                    flowForce += p.GetTransform().forward * FollowAgentsFlow * (1f - p.GetCurrentSpeed());
                }

            }
            return agents > 0 ?
            //flowForce + totalForces + proximityForce + avgDirection
            //    : Vector3.zero;
            flowForce + totalForces + proximityForce + (avgDirection / agents)
                    : Vector3.zero;
        }

        private void UpdateOrientation() {

            if (!(Navigation == NAV_STATE.DISABLED)) {
                g_OrientationAngle = Vector3.Angle(new Vector3(g_TargetOrientation.x, 0f, g_TargetOrientation.z), 
                    new Vector3(transform.forward.x,0,transform.forward.z));
                // Reduce or increment the accuracy based on the distance to final destination
                Oriented = g_OrientationAngle <= TurningAngleThreshold;
                if (!Oriented) {
                    g_NPCController.DebugRay(transform.position, g_TargetOrientation, Color.cyan);
                    LOCO_STATE d = NPCUtils.Direction(g_TargetOrientation, transform) < 1.0f ? LOCO_STATE.LEFT : LOCO_STATE.RIGHT;
                    if (g_OrientationAngle > TurningAngle)
                        Move(LOCO_STATE.IDLE);
                    Move(d);
                }
            }
        }

        private void UpdateObstaclesDetection() {
            bool detected = false;
            if (ObstaclesDetectionEnabled && Speed > 0) {
                Vector3 heightCorrection = (Vector3.up * StepColliderHeightCorrection);
                RaycastHit rayHit;
                // TODO: here we will consider all obstacles of different heights
                if (Physics.Raycast(transform.position + heightCorrection + (transform.forward * ColliderRadiusCorrection),
                        (transform.forward + Vector3.down * ColliderTestAngle), out rayHit, ColliderTestDistance)) {
                    if (!g_NPCController.Perception.IsEntityPerceived(rayHit.collider.transform)) {
                        if (rayHit.collider.bounds.extents.y <= MaxStepHeight) {
                            Transform t = rayHit.collider.transform;
                            g_Animator.SetBool("Climb_Stairs", true);
                            transform.position = Vector3.Lerp(transform.position,
                                new Vector3(transform.position.x,
                                transform.position.y + StepHeight,
                                transform.position.z), Time.deltaTime * HeightAdjustInterpolation);
                            detected = true;
                        }
                    }
                }
            }
            if (!detected) {
                if(UseSingleCapsuleCollider)
                    GetComponent<CapsuleCollider>().height = g_ColliderHeight;
                g_Animator.SetBool("Climb_Stairs", false);
                g_Blocked = false;
            }

        }

        /// <summary>
        /// Initialize all defined enum gestures by using reflection
        /// </summary>
        private void InitializeGestures() {
            Array a = Enum.GetValues(typeof(GESTURE_CODE));
            m_Gestures = new Dictionary<GESTURE_CODE, NPCAnimation>();
            Dictionary<string, NPCAnimation> npcAnimsStates = new Dictionary<string, NPCAnimation>();
            foreach (var t in a) {
                Type type = t.GetType();
                var name = Enum.GetName(type, t);
                var att = type.GetField(name).GetCustomAttributes(typeof(NPCAnimation), false);
                NPCAnimation anim = (NPCAnimation)att[0];
                anim.AnimationHash = Animator.StringToHash(anim.Name);
                m_Gestures.Add((GESTURE_CODE)t, anim);
                npcAnimsStates.Add(anim.Name, anim);
            }
            //g_NPCController.Debug("modular NPC GESTURES successfully initialized: " + m_Gestures.Count);  //This is a hacky hack
            // Fill up animation meta data in case needed in the future
            g_AnimationStates = new Dictionary<string, AnimationClip>();
            if (g_Animator != null) {
                AnimatorController controller = g_Animator.runtimeAnimatorController as AnimatorController;
                foreach (AnimatorControllerLayer layer in controller.layers) {
                    foreach (ChildAnimatorState state in layer.stateMachine.states) {
                        AnimationClip clip = state.state.motion as AnimationClip;
                        string stateName = state.state.name;
                        if (clip != null) {
                            g_AnimationStates.Add(stateName, clip);
                        }
                        if(npcAnimsStates.ContainsKey(stateName)) {
                            npcAnimsStates[stateName].AnimationClip = clip;
                            npcAnimsStates[stateName].AnimatorLayer = layer;
                            npcAnimsStates[stateName].RuntimeAnimatorLayer = Animator.GetLayerIndex(layer.name);
                            npcAnimsStates[stateName].AnimatorState = state.state;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Initialize all existing affordances
        /// </summary>
        private void InitializeAffordances() {
            Array a = typeof(NPCBody).GetMethods();
            m_Affordances = new Dictionary<NPCAffordance, string>();
            foreach (MethodInfo m in a) {
                object[] att = m.GetCustomAttributes(typeof(NPCAffordance), false);
                if (att.Length == 1) {
                    NPCAffordance aff = (NPCAffordance)att[0];
                    m_Affordances.Add(aff, aff.Name);
                }
            }
            g_NPCController.Debug("modular NPC AFFORDANCES successfully initialized: " + m_Affordances.Count);
        }

        private IEnumerator PlayAnimatedAudioClip(NPCAnimatedAudio clip) {

            if (clip.RandomizeAnimations) {
                clip.ShuffleAnimations();
            }

            Queue<NPCAnimatedAudio.AnimationStamp> anims = clip.AnimationsQueue();
            Queue<NPCAnimatedAudio.AudioClipStamp> clips = clip.AudioClipsQueue();

            float startTime = Time.time;
            float runLength = startTime + clip.Length();

            while (Time.time <= runLength) {
                if (clips.Count > 0 && (clips.Peek().ExecutionTime() + startTime) <= Time.time) {
                    g_NPCController.PlayAudioClip(clips.Dequeue().Clip);
                }
                if (anims.Count > 0 && (anims.Peek().ExecutionTime() + startTime) <= Time.time) {
                    DoGesture(anims.Dequeue().Gesture);
                }
                yield return null;
            }

            // clean up
            foreach(NPCAnimatedAudio.AnimationStamp s in clip.Animations) {
                if(m_Gestures[s.Gesture].ParamType == ANIMATION_PARAM_TYPE.BOOLEAN) {
                    DoGesture(s.Gesture, false);
                }
            }
            
        }

        #endregion
    }

}
