﻿using UnityEngine;
using System.Collections;

namespace NPC {

    public interface INPCPerceivable {
        PERCEIVEABLE_TYPE   GetNPCEntityType();
        PERCEIVE_WEIGHT     GetPerceptionWeightType();
        float               GetPerceptionWeight();
        Transform           GetTransform();
        Vector3             GetCurrentVelocity();
        float               GetCurrentSpeed();
        Vector3             GetPosition();
        Vector3             GetForwardDirection();
        float               GetAgentRadius();
        Transform           GetMainLookAtPoint();
        string              GetCurrentContext();
        void                SetCurrentContext(string c);
    }

}
